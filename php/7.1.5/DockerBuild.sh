#!/bin/sh

STAGE=ubuntu-php7.1.5-base
VERSION=20210709

docker build --no-cache -t $STAGE:$VERSION .
